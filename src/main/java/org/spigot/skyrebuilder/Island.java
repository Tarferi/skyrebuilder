package org.spigot.skyrebuilder;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Island {
	public final int id;
	public final int pos_x;
	public final int pos_y;
	public final int pos_z;
	public final int scalarpos;
	public final String owner;
	public final String ownerUUID;
	public final String date;
	public final int home_x;
	public final int home_y;
	public final int home_z;
	
	public final boolean isInsideIsland(int x, int y, int z) {
		int x_s = pos_x * Settings.offset_x;
		int z_s = pos_z * Settings.offset_z;
		int y_s = 0;
		int y_e = 255;
		int x_e = x_s + Settings.offset_x - 1;
		int z_e = z_s + Settings.offset_z - 1;
		return x>=x_s && x<=x_e && y>=y_s && y<=y_e && z>=z_s && z<=z_e;
	}
	
	public Island(ResultSet res) throws SQLException {
		id=res.getInt(1);
		owner=res.getString(2);
		ownerUUID=res.getString(3);
		pos_x=res.getInt(4);
		pos_y=res.getInt(5);
		pos_z=res.getInt(6);
		scalarpos=res.getInt(7);
		date=res.getString(8);
		home_x=res.getInt(9);
		home_y=res.getInt(10);
		home_z=res.getInt(11);
	}

	public Island(int id, int scalar,int xx, int zz, String ownername, String datecreate, int hx, int hy, int hz) {
		this.id = id;
		scalarpos = scalar;
		pos_y = Settings.offset_y;
		pos_x=xx;
		pos_z=zz;
		owner = ownername;
		date = datecreate;
		ownerUUID = null;
		home_x=hx;
		home_y=hy;
		home_z=hz;
	}

	protected int getXFromScalar(int p) {
		return p % Settings.offset_x;
	}

	protected int getZFromScalar(int p) {
		return (p / Settings.offset_x);
	}
}
