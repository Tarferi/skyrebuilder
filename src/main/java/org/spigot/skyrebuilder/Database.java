package org.spigot.skyrebuilder;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.spigot.skyrebuilder.exceptions.UserAlreadyOwnsIslandException;
import org.spigot.skyrebuilder.exceptions.UserDoesNotHaveIslandException;
import org.spigot.skyrebuilder.exceptions.UserIsNotOnHisOwnIslandException;

public class Database {
	private static String dbname = "islands.db";
	private static String path = SkyRebuilder.getInstance().getDataFolder().getAbsolutePath();
	private static Connection con;

	protected Database() {
		try {
			con = getCon();
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}
		init();
	}

	public final static void CloseDatabase() {
		try {
			con.close();
		} catch (SQLException e) {
		}
	}

	private final static Connection getCon() throws Exception {
		Class.forName("org.sqlite.JDBC");
		return DriverManager.getConnection("jdbc:sqlite:" + path + "/" + dbname);
	}

	private void init() {
		try {
			SkyRebuilder.log("Database has been oppened");
			con.createStatement().execute("CREATE TABLE IF NOT EXISTS 'Islands' (id INTEGER PRIMARY KEY AUTOINCREMENT,owner varchar(20) NOT NULL, ownerUUID varchar(50), X int, Y int, Z int, scalar int,createdate varchar(15),home_x int, home_y int, home_z int)");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static final Island getIsland(String username) throws UserDoesNotHaveIslandException {
		try {
			ResultSet r = con.createStatement().executeQuery("SELECT * FROM Islands WHERE owner='" + username.toLowerCase() + "'");
			if (r.next()) {
				return new Island(r);
			}
		} catch (Exception e) {
		}
		throw new UserDoesNotHaveIslandException();
	}

	public static final Location getHomeLocation(Player p) throws UserDoesNotHaveIslandException {
		Island I = Database.getIsland(p.getName());
		int x, y, z;
		if (I.home_x == 0 && I.home_y == 0 && I.home_z == 0) {
			x = I.pos_x * Settings.offset_x + Settings.home_X;
			y = Settings.home_Y;
			z = I.pos_z * Settings.offset_z + Settings.home_Z;
		} else {
			x = I.pos_x * Settings.offset_x + I.home_x;
			y = I.home_y;
			z = I.pos_z * Settings.offset_z + I.home_z;
		}
		return new Location(Settings.World, x, y, z);
	}

	private static final int getNextIslandIndex() {
		try {
			ResultSet r = con.createStatement().executeQuery("SELECT * FROM Islands ORDER BY scalar DESC LIMIT 1");
			if (r.next()) {
				return new Island(r).scalarpos + 1;
			} else {
				return 0;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}

	private static final void addNewIsland(Island i) {
		try {
			con.createStatement().execute("Insert INTO Islands (owner,ownerUUID,X,Y,Z,scalar,createdate,home_x,home_y,home_z) VALUES ('" + i.owner + "','" + i.ownerUUID + "','" + i.pos_x + "','" + i.pos_y + "','" + i.pos_z + "','" + i.scalarpos + "','" + i.date + "','" + i.home_x + "','" + i.home_y + "','" + i.home_z + "')");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static final void updateHomePosition(int x, int y, int z, String player) throws UserDoesNotHaveIslandException, UserIsNotOnHisOwnIslandException {
		Island i = Database.getIsland(player);
		if (!i.isInsideIsland(x, y, z)) {
			throw new UserIsNotOnHisOwnIslandException();
		}
		x = x % Settings.offset_x;
		z = z % Settings.offset_z;
		try {
			con.createStatement().execute("UPDATE Islands set home_x='" + x + "', home_y='" + y + "', home_z='" + z + "' WHERE id='" + i.id + "'");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static final synchronized void addIslandForUser(String username) throws UserAlreadyOwnsIslandException {
		try {
			getIsland(username);
			throw new UserAlreadyOwnsIslandException();
		} catch (UserDoesNotHaveIslandException e) {
			addIslandForUser(username, getNewIsland(username));
		}
	}

	public static final synchronized void addIslandForUser(String username, Island i) throws UserAlreadyOwnsIslandException {
		try {
			getIsland(username);
			throw new UserAlreadyOwnsIslandException();
		} catch (UserDoesNotHaveIslandException e) {
			addNewIsland(getNewIsland(username));
		}
	}

	protected static final synchronized Island getNewIsland(String username) throws UserAlreadyOwnsIslandException {
		try {
			getIsland(username);
			throw new UserAlreadyOwnsIslandException();
		} catch (UserDoesNotHaveIslandException e) {
			int scalar = getNextIslandIndex();
			int[] coords = getNextCoords(scalar);
			int x = coords[0];
			int z = coords[1];
			return new Island(0, scalar, x, z, username, SkyRebuilder.getCurrentTime(), 0, 0, 0);
		}
	}

	protected void getLastIsland() {

	}

	private final static int[] getNextCoords(int scalar) {
		if (scalar == 0) {
			return new int[] { 0, 0 };
		}
		int index = 0;
		for (int i = 1; true; i++) {
			int step = i * 2;
			index += step;
			if (scalar < index) {
				return new int[] { i, -i + (index - scalar) };
			}
			index += step;
			if (scalar < index) {
				return new int[] { -i + (index - scalar), -i };
			}
			index += step;
			if (scalar < index) {
				return new int[] { -i, i - (index - scalar) };
			}
			index += step;
			if (scalar < index) {
				return new int[] { i - (index - scalar), i };
			}
		}
	}
}
