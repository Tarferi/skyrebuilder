package org.spigot.skyrebuilder;

import java.util.ArrayList;
import java.util.List;

import net.t00thpick1.residence.api.ResidenceAPI;
import net.t00thpick1.residence.api.ResidenceManager;
import net.t00thpick1.residence.api.areas.CuboidArea;
import net.t00thpick1.residence.api.areas.ResidenceArea;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.spigot.skyrebuilder.exceptions.SchematicFileDoesNotExistException;
import org.spigot.skyrebuilder.exceptions.SchematicTooBigException;
import org.spigot.skyrebuilder.exceptions.UserAlreadyHasIslandUnderConstructionException;
import org.spigot.skyrebuilder.exceptions.UserAlreadyOwnsIslandException;
import org.spigot.skyrebuilder.exceptions.UserDoesNotHaveIslandException;

public class Queue {
	private List<Job> jobs = new ArrayList<>();
	private Object working = new Object();
	private Worker worker;

	protected void respond(CommandSender sender, String message) {
		if (sender != null) {
			try {
				SkyRebuilder.respond(sender, message);
			} catch (Exception e) {
			}
		}
	}

	public void addJob(JOBS job, String username, CommandSender sender, Island Island) throws UserAlreadyHasIslandUnderConstructionException {
		synchronized (jobs) {
			for (Job jobb : jobs) {
				if (jobb.Username.equals(username)) {
					throw new UserAlreadyHasIslandUnderConstructionException();
				}
			}
			jobs.add(new Job(job, username.toLowerCase(), Island));
		}
		if (!tryNextJob()) {
			if (Settings.msg_queue_add_enabled) {
				respond(sender, Settings.msg_queue_add_message.replace("%c", "" + jobs.size()));
			}
		}
	}

	public boolean hasJob(String username) {
		username = username.toLowerCase();
		synchronized (jobs) {
			for (Job job : jobs) {
				if (job.Username.equals(username)) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean tryNextJob() {
		synchronized (jobs) {
			synchronized (working) {
				if (jobs.size() == 1 && worker == null) {
					Job j = jobs.get(0);
					worker = new Worker(j);
					worker.runTaskAsynchronously(SkyRebuilder.getInstance());
					return true;
				} else {
					return false;
				}
			}
		}
	}

	private class Worker extends BukkitRunnable {
		private Job job;

		private Worker(Job j) {
			job = j;
		}

		@Override
		public void run() {
			Player p = Bukkit.getPlayer(job.Username);
			if (Settings.msg_build_start_enabled) {
				respond(p, Settings.msg_build_start_message);
			}
			JOBS j = job.Job;
			if (j == JOBS.CLEAN) {

			} else {
				try {
					Database.getIsland(job.Username);
					if (Settings.msg_build_fail_enabled) {
						respond(p, Settings.msg_build_fail_message);
					}
				} catch (UserDoesNotHaveIslandException ee) {
					Island i = job.Island;
					int x_s = i.pos_x * Settings.offset_x;
					int z_s = i.pos_z * Settings.offset_z;
					try {
						Schematic.getSchematic().build(x_s, i.pos_y, z_s);
					} catch (SchematicTooBigException e1) {
						respond(p,"�4Zadan� sch�ma je v�t�� ne� vymezen� m�sto!");
						return;
					} catch (SchematicFileDoesNotExistException e) {
						respond(p,"�4Zadan� sch�ma nebylo nalezeno!");
						return;
					}
					try {
						Database.addIslandForUser(job.Username, i);
					} catch (UserAlreadyOwnsIslandException e) {
					}
					if (Settings.msg_build_finish_enabled) {
						respond(p, Settings.msg_build_finish_message);
					}
					createResidence(i, p);
				}
				synchronized (jobs) {
					jobs.remove(0);
					if (jobs.size() == 0) {
						worker = null;
					} else {
						Job jj = jobs.get(0);
						worker = new Worker(jj);
						worker.runTaskAsynchronously(SkyRebuilder.getInstance());
					}
				}
			}
		}
	}

	@SuppressWarnings("deprecation")
	private void createResidence(Island i, CommandSender p) {
		int x_s = i.pos_x * Settings.offset_x;
		int z_s = i.pos_z * Settings.offset_z;
		int y_s = 0;
		int y_e = 255;
		int x_e = x_s + Settings.offset_x - 1;
		int z_e = z_s + Settings.offset_z - 1;
		Location l1 = new Location(Settings.World, x_s, y_s, z_s);
		Location l2 = new Location(Settings.World, x_e, y_e, z_e);
		CuboidArea newArea = ResidenceAPI.createCuboidArea(l1, l2);
		ResidenceManager resmanager = ResidenceAPI.getResidenceManager();
		String resname = Settings.resname.replace("%owner", i.owner);
		if (resmanager.getByName(resname) != null) {
			resmanager.remove(resmanager.getByName(resname));
		}
		ResidenceArea res = resmanager.createResidence(resname, i.owner, newArea);
		if (res == null) {
			if (Settings.msg_res_enabled) {
				SkyRebuilder.respond(p, Settings.msg_res_fail_message);
			}
		} else {
			SkyRebuilder.refreshResidenceFlags(res);
			if (Settings.msg_res_enabled) {
				SkyRebuilder.respond(p, Settings.msg_res_ok_message);
			}
		}
	}

	private class Job {
		private final JOBS Job;
		private final String Username;
		private final Island Island;

		private Job(JOBS job, String username, Island island) {
			Job = job;
			Username = username;
			Island = island;
		}
	}

	public void printJobs(CommandSender sender) {
		if (Settings.msg_queue_list_enabled) {
			synchronized (jobs) {
				if (jobs.isEmpty()) {
					SkyRebuilder.respond(sender, Settings.msg_queue_empty_message);
				} else {
					SkyRebuilder.respond(sender, Settings.msg_queue_list_message);
					int i = 0;
					for (Job job : jobs) {
						SkyRebuilder.respond(sender, "�7" + i + ": �b" + job.Username);
						i++;
					}
				}
			}
		}
	}
}

enum JOBS {
	BUILD, CLEAN
}