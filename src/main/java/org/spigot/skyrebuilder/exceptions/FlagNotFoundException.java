package org.spigot.skyrebuilder.exceptions;

public class FlagNotFoundException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public FlagNotFoundException(String text) {
		super(text);
	}
}
