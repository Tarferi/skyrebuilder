package org.spigot.skyrebuilder;

import java.util.HashMap;

import net.t00thpick1.residence.api.flags.Flag;
import net.t00thpick1.residence.api.flags.FlagManager;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.spigot.skyrebuilder.exceptions.FlagNotFoundException;

public class Settings {
	public static String world;
	public static int offset_x;
	public static int offset_y;
	public static int offset_z;
	/*
	 * public static int size_x=200; public static int size_z=200;
	 */
	public static int home_X;
	public static int home_Y;
	public static int home_Z;
	public static String schema;
	public static World World;
	public static int delay = 10;
	public static int batch = 100;
	public static String msg_enter;
	public static String msg_leave;
	public static Database db;
	public static String msg_queue_add_message;
	public static boolean msg_queue_add_enabled;
	public static String msg_build_start_message;
	public static boolean msg_build_start_enabled;
	public static String msg_build_fail_message;
	public static boolean msg_build_fail_enabled;
	public static String msg_build_finish_message;
	public static boolean msg_build_finish_enabled;
	public static String resname;
	public static String msg_queue_list_message;
	public static String msg_queue_empty_message;
	public static boolean msg_queue_list_enabled;
	public static String msg_res_ok_message;
	public static String msg_res_fail_message;
	public static boolean msg_res_enabled;
	public static String msg_no_perms;
	public static String msg_welcome_home;
	public static String msg_no_island;
	public static String msg_has_island;
	public static String msg_invalid_params;
	public static String msg_has_island_construction;
	public static String msg_global_home;
	public static String msg_help;
	public static String msg_help_auto;
	public static String msg_help_home;
	public static String msg_help_create;
	public static String msg_help_jobs;
	public static String msg_help_globalhome;
	public static String msg_help_reload;
	public static String msg_settings_loaded;
	public static String msg_settings_failed;
	public static String msg_new_home;
	public static HashMap<Flag, Boolean> flag_world;
	public static HashMap<Flag, Boolean> flag_island;
	public static HashMap<Flag, Boolean> flag_island_owner;
	public static String msg_help_sethome;
	public static String msg_new_homefail;

	public static void loadSettings() throws Exception {
		SkyRebuilder.getInstance().saveDefaultConfig();
		FileConfiguration c = SkyRebuilder.getInstance().getConfig();
		world = c.getString("World");
		/*
		 * size_x=c.getInt("Size X"); size_z=c.getInt("Size Z");
		 */

		msg_res_ok_message = c.getString("Msg-res-ok-message");
		msg_res_fail_message = c.getString("Msg-res-fail-message");
		msg_res_enabled = c.getBoolean("Msg-res-enabled");
		msg_queue_empty_message = c.getString("Msg-queue-empty-message");
		msg_queue_list_message = c.getString("Msg-queue-list-message");
		msg_queue_list_enabled = c.getBoolean("Msg-queue-list-enabled");
		msg_build_finish_message = c.getString("Msg-build-finished-message");
		msg_build_finish_enabled = c.getBoolean("Msg-build-finished-enabled");
		msg_build_fail_message = c.getString("Msg-build-fail-message");
		msg_build_fail_enabled = c.getBoolean("Msg-build-fail-enabled");
		msg_build_start_message = c.getString("Msg-build-start-message");
		msg_build_start_enabled = c.getBoolean("Msg-build-start-enabled");
		msg_queue_add_message = c.getString("Msg-queue-add-message");
		msg_queue_add_enabled = c.getBoolean("Msg-queue-add-enabled");
		msg_no_perms = c.getString("Msg-no-perms");
		msg_welcome_home = c.getString("Msg-home");
		msg_no_island = c.getString("Msg-no-island");
		msg_has_island = c.getString("Msg-has-island");
		msg_has_island_construction = c.getString("Msg-has-island-construction");
		msg_invalid_params = c.getString("Msg-invalid-cmd");
		resname = c.getString("Res-name");
		msg_global_home = c.getString("Msg-global-home");
		msg_help = c.getString("Msg_help");
		msg_help_auto = c.getString("Msg_help_auto");
		msg_help_home = c.getString("Msg_help_home");
		msg_help_create = c.getString("Msg_help_create");
		msg_help_jobs = c.getString("Msg_help_jobs");
		msg_help_globalhome = c.getString("Msg_help_setglobalhome");
		msg_help_reload = c.getString("Msg_help_reload");
		msg_settings_loaded = c.getString("Msg-settings-loaded");
		msg_settings_failed = c.getString("Msg-settings-failed");
		home_X = c.getInt("home X");
		home_Y = c.getInt("home Y");
		home_Z = c.getInt("home Z");
		offset_x = c.getInt("Offset X");
		offset_y = c.getInt("Offset Y");
		offset_z = c.getInt("Offset Z");
		schema = c.getString("Schematic file");
		World = Bukkit.getWorld(world);
		delay = c.getInt("Delay");
		batch = c.getInt("Batch");
		msg_enter = c.getString("Enter-Message");
		msg_leave = c.getString("Leave-Message");
		flag_world = getList(c.getString("flags_world"));
		flag_island = getList(c.getString("flags_island"));
		flag_island_owner = getList(c.getString("flags_island_owner"));
		msg_new_home=c.getString("Msg-new-home");
		msg_new_homefail=c.getString("Msg-new-homefial");
		msg_help_sethome=c.getString("Msg_help_sethome");
		if (World == null) {
			throw new Exception("World '" + world + "' was not found");
		}
		SkyRebuilder.refreshWorldFlags();
		saveSettings();
	}

	private static HashMap<Flag, Boolean> getList(String s) throws FlagNotFoundException {
		s=s.trim();
		String[] ss=s.split(",");
		HashMap<Flag, Boolean> f = new HashMap<>();
		for (String o : ss) {
			o=o.trim();
			boolean b = true;
			if (o.startsWith("!")) {
				b = false;
				o = o.substring(1);
			}
			if (FlagManager.getFlag(o) != null) {
				f.put(FlagManager.getFlag(o),b);
			} else {
				throw new FlagNotFoundException("Flag '" + o + "' was not found");
			}
		}
		return f;
	}

	public static void saveSettings() {
		FileConfiguration c = SkyRebuilder.getInstance().getConfig();
		c.set("World", world);
		/*
		 * c.set("Size X", size_x); c.set("Size Z", size_z);
		 */
		c.set("Offset X", offset_x);
		c.set("Offset Y", offset_y);
		c.set("Offset Z", offset_z);
		c.set("Delay", delay);
		c.set("Batch", batch);
		c.set("Schematic file", schema);
		c.set("home X", home_X);
		c.set("home Y", home_Y);
		c.set("home Z", home_Z);
		SkyRebuilder.getInstance().saveConfig();
	}
}
