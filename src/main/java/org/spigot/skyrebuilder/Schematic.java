package org.spigot.skyrebuilder;

import java.io.File;
import java.io.FileInputStream;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.jnbt.CompoundTag;
import org.jnbt.NBTInputStream;
import org.jnbt.Tag;
import org.spigot.skyrebuilder.exceptions.SchematicFileDoesNotExistException;
import org.spigot.skyrebuilder.exceptions.SchematicTooBigException;

public class Schematic {
	private static String path = SkyRebuilder.getInstance().getDataFolder().getAbsolutePath();
	public final int max_x;
	public final int max_y;
	public final int max_z;
	public final int totalsize;
	public final short[] SolidBlocks;
	public final short[] ItemBlocks;
	public final byte[] DataBlocks;
	private int off_x = 0;
	private int off_y = 0;
	private int off_z = 0;
	private boolean finishedSolidblocks = false;
	private boolean finishedItemBlocks = false;
	private Object sync = new Object();

	public synchronized void build(int x, int y, int z) {
		off_x += x;
		off_y += y;
		off_z += z;
		int index = -1;
		while (!finishedSolidblocks) {
			int[] todo = new int[Settings.batch];
			int i;
			for (i = 0; i < Settings.batch && index < this.totalsize; i++) {
				index = getNextNonZeroSolidBlock(index);
				if (index >= totalsize || index < 0) {
					break;
				} else {
					todo[i] = index;
				}
			}
			synchronized (sync) {
				commit(SolidBlocks, todo, sync);
				try {
					sync.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			try {
				wait(Settings.delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return;
			}
			if (index >= totalsize || index == -1) {
				finishedSolidblocks = true;
			}
		}
		try {
			wait(1000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		while (!finishedItemBlocks) {
			int[] todo = new int[Settings.batch];
			int i;
			for (i = 0; i < Settings.batch && index < this.totalsize; i++) {
				index = getNextNonZeroItemBlock(index);
				if (index >= totalsize || index < 0) {
					break;
				} else {
					todo[i] = index;
				}
			}
			synchronized (sync) {
				commit(ItemBlocks, todo, sync);
				try {
					sync.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			try {
				wait(Settings.delay);
			} catch (InterruptedException e) {
				e.printStackTrace();
				return;
			}
			if (index >= totalsize || index == -1) {
				finishedItemBlocks = true;
			}
		}
	}

	protected int getXFromScalarAbs(int p) {
		return (p % max_x) + off_x;
	}

	protected int getYFromScalarAbs(int p) {
		return (p / (max_z * max_x)) + off_y;
	}

	protected int getZFromScalarAbs(int p) {
		return ((p / max_x) % max_z) + off_z;
	}

	@SuppressWarnings("deprecation")
	private void commit(final short[] solidBlocks, final int[] todo, final Object sync) {
		Bukkit.getScheduler().runTask(SkyRebuilder.getInstance(), new Runnable() {
			@Override
			public void run() {
				try {
					org.bukkit.World w = Settings.World;
					for (int o = 0; o < Settings.batch; o++) {
						int i = todo[o];
						if (i == -1) {
							break;
						}
						short id = solidBlocks[i];
						short data = DataBlocks[i];
						Block b = w.getBlockAt(getXFromScalarAbs(i), getYFromScalarAbs(i), getZFromScalarAbs(i));
						b.setTypeId(id);
						b.setData((byte) data, false);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
				synchronized (sync) {
					sync.notify();
				}
			}
		});
	}

	private int getNextNonZeroSolidBlock(int z) {
		while (z < totalsize - 1) {
			z++;
			if (SolidBlocks[z] != 0) {
				return z;
			}
		}
		return -1;
	}

	private int getNextNonZeroItemBlock(int z) {
		while (z < totalsize - 1) {
			z++;
			if (ItemBlocks[z] != 0) {
				return z;
			}
		}
		return -1;
	}

	private final static boolean isItemBlocks(int id) {
		switch (id) {
			default:
				return false;
			case 6:
			case 8:
			case 9:
			case 10:
			case 11:
			case 26:
			case 27:
			case 28:
			case 31:
			case 32:
			case 37:
			case 38:
			case 39:
			case 40:
			case 50:
			case 51:
			case 55:
			case 59:
			case 63:
			case 64:
			case 65:
			case 66:
			case 68:
			case 69:
			case 70:
			case 71:
			case 72:
			case 75:
			case 76:
			case 77:
			case 78:
			case 81:
			case 83:
			case 90:
			case 92:
			case 93:
			case 94:
			case 96:
			case 104:
			case 105:
			case 106:
			case 107:
			case 111:
			case 115:
			case 117:
			case 119:
			case 122:
			case 127:
			case 131:
			case 132:
			case 141:
			case 142:
			case 143:
			case 144:
			case 145:
			case 147:
			case 148:
			case 149:
			case 150:
			case 157:
			case 171:
			case 175:
			case 176:
			case 193:
			case 194:
			case 195:
			case 196:
			case 197:
				return true;
		}
	}

	public static Schematic getSchematic() throws SchematicTooBigException, SchematicFileDoesNotExistException {
		int tmax_x = 0;
		int tmax_y = 0;
		int tmax_z = 0;
		int toff_x = 0;
		int toff_z = 0;
		int ttotalsize = 0;
		short[] tsblocks = null;
		short[] tiblocks = null;
		byte[] datablocks = null;
		try {
			File f = new File(path + "/" + Settings.schema);
			if (f.exists()) {
				FileInputStream fs = new FileInputStream(f);
				NBTInputStream ns = null;
				ns = new NBTInputStream(fs);
				Tag t = ns.readTag();
				if (t.getName().toLowerCase().equals("schematic")) {
					if (t instanceof CompoundTag) {
						CompoundTag tg = (CompoundTag) t;
						Map<String, Tag> vals = tg.getValue();
						tmax_x = (short) vals.get("Width").getValue();
						tmax_y = (short) vals.get("Height").getValue();
						tmax_z = (short) vals.get("Length").getValue();
						toff_x = (Settings.offset_x - tmax_x) / 2;
						toff_z = (Settings.offset_z - tmax_z) / 2;
						if (toff_x < 0 || toff_z < 0) {
							try {
								ns.close();
								fs.close();
							} catch (Exception e) {
								e.printStackTrace();
							}
							throw new SchematicTooBigException();
						}
						ttotalsize = tmax_x * tmax_y * tmax_z;
						tsblocks = new short[ttotalsize];
						tiblocks = new short[ttotalsize];
						byte[] id = (byte[]) vals.get("Blocks").getValue();
						datablocks = (byte[]) vals.get("Data").getValue();
						for (int i = 0; i < ttotalsize; i++) {
							if (isItemBlocks(id[i])) {
								tiblocks[i] = (short) (id[i] & 0xff);
								tsblocks[i] = 0;
							} else {
								tiblocks[i] = 0;
								tsblocks[i] = (short) (id[i] & 0xff);
							}
						}
						ns.close();
						fs.close();
					}
				}
			} else {
				throw new SchematicFileDoesNotExistException();
			}
		} catch (SchematicFileDoesNotExistException eee) {
			throw eee;
		} catch (SchematicTooBigException ee) {
			throw ee;
		} catch (Exception e) {
			e.printStackTrace();
		}
		/*
		 * max_x = tmax_x; max_y = tmax_y; max_z = tmax_z; totalsize =
		 * ttotalsize; SolidBlocks = tsblocks; ItemBlocks = tiblocks; DataBlocks
		 * = datablocks;
		 */
		return new Schematic(tmax_x, tmax_y, tmax_z, ttotalsize, tsblocks, tiblocks, datablocks, toff_x, toff_z);
	}

	private Schematic(int max_x, int max_y, int max_z, int totalsize, short[] SolidBlocks, short[] ItemBlocks, byte[] DataBlocks, int off_x, int off_z) {
		this.max_x = max_x;
		this.max_y = max_y;
		this.max_z = max_z;
		this.totalsize = totalsize;
		this.SolidBlocks = SolidBlocks;
		this.ItemBlocks = ItemBlocks;
		this.DataBlocks = DataBlocks;
		this.off_x = off_x;
		this.off_z = off_z;
	}

}
