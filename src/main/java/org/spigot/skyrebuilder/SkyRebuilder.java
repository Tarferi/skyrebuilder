package org.spigot.skyrebuilder;

import java.util.Date;
import java.util.logging.Level;

import net.minecraft.util.org.apache.commons.lang3.StringUtils;
import net.t00thpick1.residence.api.ResidenceAPI;
import net.t00thpick1.residence.api.areas.ResidenceArea;
import net.t00thpick1.residence.api.flags.Flag;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigot.skyrebuilder.exceptions.UserAlreadyHasIslandUnderConstructionException;
import org.spigot.skyrebuilder.exceptions.UserAlreadyOwnsIslandException;
import org.spigot.skyrebuilder.exceptions.UserDoesNotHaveIslandException;
import org.spigot.skyrebuilder.exceptions.UserIsNotOnHisOwnIslandException;

public class SkyRebuilder extends JavaPlugin {
	private static SkyRebuilder instance;
	public static final Queue Queue = new Queue();

	public static SkyRebuilder getInstance() {
		return instance;
	}

	public SkyRebuilder() {
		instance = this;
	}

	protected static void respond(CommandSender sender, String message) {
		if (sender != null) {
			if ((sender instanceof Player)) {
				((Player) sender).sendMessage(message);
			} else {
				Bukkit.getConsoleSender().sendMessage(StringUtils.stripAccents(message));
			}
		}
	}

	private boolean allowed(CommandSender sender, String permission, boolean allowconsole) {
		return sender instanceof Player ? (((Player) sender).hasPermission(permission) || ((Player) sender).isOp()) : allowconsole;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().toLowerCase().equals("is") || cmd.getName().toLowerCase().equals("island")) {
			if (args.length == 1) {
				String com = args[0].toLowerCase();
				switch (com) {
					default:
						respond(sender, Settings.msg_invalid_params);
					break;
					case "home":
						if (!allowed(sender, "is.home", false)) {
							respond(sender, Settings.msg_no_perms);
							break;
						}
						sendHome((Player) sender);
					break;
					case "setglobalhome":
						if (!allowed(sender, "is.setglobalhome", false)) {
							respond(sender, Settings.msg_no_perms);
							break;
						}
						setGlobalHomeLocation((Player) sender);
						respond(sender, Settings.msg_global_home);
					break;
					case "auto":
						if (!allowed(sender, "is.auto", false)) {
							respond(sender, Settings.msg_no_perms);
							break;
						}
						try {
							Database.getIsland(((Player) sender).getName());
							sendHome((Player) sender);
						} catch (UserDoesNotHaveIslandException e) {
							tryAddIsland(sender, ((Player) sender).getName());
						}
					break;
					case "create":
						if (!allowed(sender, "is.create", false)) {
							respond(sender, Settings.msg_no_perms);
							break;
						}
						try {
							Database.getIsland(((Player) sender).getName());
							respond(sender, Settings.msg_has_island);
						} catch (UserDoesNotHaveIslandException e) {
							tryAddIsland(sender, ((Player) sender).getName());
						}
					break;
					case "jobs":
						if (!allowed(sender, "is.jobs", true)) {
							respond(sender, Settings.msg_no_perms);
							break;
						}
						Queue.printJobs(sender);
					break;
					case "sethome":
						if (!allowed(sender, "is.sethome", false)) {
							respond(sender, Settings.msg_no_perms);
							break;
						}
						Location l = ((Player) sender).getLocation();
						try {
							Database.updateHomePosition(l.getBlockX(), l.getBlockY(), l.getBlockZ(), ((Player) sender).getName());
							respond(sender, Settings.msg_new_home);
						} catch (UserDoesNotHaveIslandException e1) {
							respond(sender, Settings.msg_no_island);
						} catch (UserIsNotOnHisOwnIslandException e) {
							respond(sender, Settings.msg_new_homefail);
						}
					break;
					case "reload":
						if (!allowed(sender, "is.reload", true)) {
							respond(sender, Settings.msg_no_perms);
							break;
						}
						try {
							Settings.loadSettings();
						} catch (Exception e) {
							e.printStackTrace();
							respond(sender, Settings.msg_settings_failed);
						}
						respond(sender, Settings.msg_settings_loaded);
					break;
				}
			} else if (args.length == 0) {
				respond(sender, "�2Pou�it�:\n" + Settings.msg_help);
				if (allowed(sender, "is.auto", true)) {
					respond(sender, Settings.msg_help_auto);
				}
				if (allowed(sender, "is.create", true)) {
					respond(sender, Settings.msg_help_create);
				}
				if (allowed(sender, "is.home", true)) {
					respond(sender, Settings.msg_help_home);
				}
				if (allowed(sender, "is.jobs", true)) {
					respond(sender, Settings.msg_help_jobs);
				}
				if (allowed(sender, "is.setglobalhome", true)) {
					respond(sender, Settings.msg_help_globalhome);
				}
				if (allowed(sender, "is.reload", true)) {
					respond(sender, Settings.msg_help_reload);
				}
				if (allowed(sender, "is.sethome", true)) {
					respond(sender, Settings.msg_help_sethome);
				}

			} else {
				respond(sender, Settings.msg_invalid_params);
			}
		}
		return false;
	}

	public static final void sendHome(Player p) {
		try {
			p.teleport(Database.getHomeLocation(p));
			respond(p, Settings.msg_welcome_home);
		} catch (UserDoesNotHaveIslandException e) {
			respond(p, Settings.msg_no_island);
		}
	}

	public static final void setGlobalHomeLocation(Player p) {
		Location loc = p.getLocation();
		Settings.home_X = loc.getBlockX() % Settings.offset_x;
		Settings.home_Y = loc.getBlockY();
		Settings.home_Z = loc.getBlockZ() % Settings.offset_z;
		Settings.saveSettings();
	}

	private void tryAddIsland(CommandSender sender, String username) {
		try {
			if (Queue.hasJob(username)) {
				respond(sender, Settings.msg_has_island_construction);
			} else {
				Island i = Database.getNewIsland(username);
				Queue.addJob(JOBS.BUILD, username, sender, i);
			}
		} catch (UserAlreadyOwnsIslandException e) {
			respond(sender, Settings.msg_has_island);
		} catch (UserAlreadyHasIslandUnderConstructionException e) {
			respond(sender, Settings.msg_has_island_construction);
		}

	}

	@Override
	public void onEnable() {
		try {
			Settings.loadSettings();
			Settings.db = new Database();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static final void refreshWorldFlags() {
		for (Flag flag : Settings.flag_world.keySet()) {
			ResidenceAPI.getResidenceWorld(Settings.World).setAreaFlag(flag, Settings.flag_world.get(flag));
		}
	}

	public static final String getCurrentTime() {
		return new Date().getTime() / 1000 + "";
	}

	@SuppressWarnings("deprecation")
	public static final void refreshResidenceFlags(ResidenceArea res) {
		for (Flag flag : Settings.flag_island.keySet()) {
			res.setAreaFlag(flag, Settings.flag_island.get(flag));
		}
		for (Flag flag : Settings.flag_island_owner.keySet()) {
			res.setPlayerFlag(res.getOwner(), flag, Settings.flag_island_owner.get(flag));
		}
		res.setEnterMessage(Settings.msg_enter);
		res.setLeaveMessage(Settings.msg_leave);
	}

	public void onDisable() {
		Database.CloseDatabase();
	}

	public static final void log(String message) {
		instance.getLogger().log(Level.INFO, message);
	}
}
